const binance = require('node-binance-api');
const Ichimoku = require('ichimoku');
const path = require('path');
const fs = require('fs');
const mailgun = require('mailgun.js');
require('dotenv').config();

const CHANNEL = "BTCUSDT";
const INTERVAL = process.env.INTERVAL;
const QUANTITY = process.env.QUANTITY;
const MAKE_TRADE = process.env.TRADE === 'true';

const mg = mailgun.client({
    username: 'ademcirak',
    key: process.env.MAILGUN_API_KEY,
    public_key: process.env.MAILGUN_PUBLIC_KEY
});

const ichimoku = new Ichimoku({
    conversionPeriod: 9,
    basePeriod: 26,
    spanPeriod: 52,
    displacement: 26,
    values: []
});


binance.options({
    APIKEY: process.env.BINANCE_API_KEY,
    APISECRET: process.env.BINANCE_API_SECRET,
    useServerTime: true, // If you get timestamp errors, synchronize to server time at startup
    test: true // If you want to use sandbox mode where orders are simulated
});

const startTime = new Date().getMilliseconds();
let lastOperation = new Date().getMilliseconds();
let isTrading = false;

function nl2br(str, isXhtml) {
    const breakTag = isXhtml ? '<br />' : '<br>';
    return String(str).replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

const checkAndMakeTrade = (data, operation) => {
    if(isTrading) {
        console.log('Trading...');
    }
    if(data.date < startTime) {
        // console.log('before start time')
    }
    else if(data.date < lastOperation) {
        // console.log('before last operation ' + lastOperation);
    }
    else {
        console.log('Making operation: ' + operation, data);

        let mail = '<p>Making operation: ' + operation + '</p><p>Data: </p>';
        mail+= nl2br(JSON.stringify(data, null, 4));

        mg.messages.create(process.env.MAILGUN_DOMAIN, {
            from: 'Trade Bot <postmaster@' + process.env.MAILGUN_DOMAIN + '>',
            to: (process.env.EMAIL_RECEIVER || '').split(','),
            subject: "Trade operation is triggered",
            text: "Trade operation is triggered",
            html: mail
        })
            .then(msg => console.log('Mail sent:', msg)) // logs response data
            .catch(err => console.log('Mail error: ', err)); // logs any error


        if(MAKE_TRADE) {
            isTrading = true;
            if(operation === 'buy') {
                binance.marketBuy(CHANNEL, QUANTITY, (err, result) => {
                    isTrading = false;
                    if(err) {
                        console.error('Trade error:', err)
                    } else {
                        const tradeDate = new Date();
                        lastOperation = tradeDate.getMilliseconds();
                        console.log('Trade is done: ', result);
                    }
                });
            } else if(operation === 'sell') {
                binance.marketSell(CHANNEL, QUANTITY, (err, result) => {
                    isTrading = false;
                    if(err) {
                        console.error('Trade error:', err)
                    } else {
                        const tradeDate = new Date();
                        lastOperation = tradeDate.getMilliseconds();
                        console.log('Trade is done: ', result);
                    }
                });
            }
        }
    }
};

binance.websockets.chart(CHANNEL, INTERVAL, (symbol, interval, chart) => {
    let tick = binance.last(chart);
    const last = chart[tick].close;
    const lastPriceDate = new Date(parseInt(tick));
    const ichiValues = [];

    console.log(lastPriceDate + ' Close price for ' + CHANNEL + ': ' + last);

    const recordCount = Object.keys(chart).length;
    const lastRecords = Object.keys(chart).slice(recordCount.length - 200, recordCount.length);

    lastRecords.forEach((time) => {
        const date = new Date(parseInt(time));
        const d = chart[time];

        let ichimokuValue = ichimoku.nextValue({
            high: d.high,
            low: d.low,
            close: d.close
        });

        ichiValues.push({ date: date.getMilliseconds(), data: d,...ichimokuValue});
    });

    let isGreen = ichiValues[0].spanA > ichiValues[0].spanB;
    ichiValues.forEach((value) => {

        const nextIsGreen = value.spanA > value.spanB;

        if(isGreen && !nextIsGreen) {
            checkAndMakeTrade(value, 'sell');
        }
        else if(!isGreen && nextIsGreen) {
            checkAndMakeTrade(value, 'buy');
        }
        isGreen = nextIsGreen;
    });
});
